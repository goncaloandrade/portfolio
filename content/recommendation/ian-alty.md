---
layout: recommendation
title: Ian Alty
date: 2016-06-02 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601993338/Profile%20Pictures/Ian_Alty.jpg
role: Web Software Developer
job: Orbus
linkedin: https://www.linkedin.com/in/ian-a-alty
---

I worked with Gonçalo and found him to be a great designer. I recommend him to anyone looking for a ui/ux designer.
