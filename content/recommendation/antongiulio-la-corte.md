---
layout: recommendation
title: Antonguilio La Corte
date: 2018-09-30 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601993338/Profile%20Pictures/Antongiulio_La_Corte.jpg
role: Product Owner
job: Croud
linkedin: https://www.linkedin.com/in/lacortea/
---

We hired Goncalo in a freelance capacity to lead a critical phase of UX re-design for a b2b web app, as part of a larger project involving approx 15 people across 3 Product & Technology teams. He successfully delivered prototype solutions (navigation, information architecture, workflow) to improve usability in several key areas, liaising extensively with users and stakeholders throughout the process.
