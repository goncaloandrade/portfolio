---
layout: portfolio
title: ComPM
date: 2013-03-21 10:45:06 +0000
categories: null
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929662/ComPM/compm-featured.png
color: "#142b52"
keywords:
  - Logo
  - Graphic Design
---
Inova+, A consultancy that helped other companies do projects for the European Union. This involved, amongst many other things, graphic design for all their communication materials. I helped Inova+ design logos and identities to use with these on several projects.

# Framework branding

This was a branding project for a European framework for project managers. The project came from the agency I was freelancing with at the time, [Inovamais](http://inovamais.eu/). They decided to contract me themselves to work on the identity of this product.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929662/ComPM/compm-1.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929662/ComPM/compm-2.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929662/ComPM/compm-3.jpg)

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929662/ComPM/compm-4.jpg)