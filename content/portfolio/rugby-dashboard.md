---
layout: portfolio
title: Rugby Dashboard
date: 2015-07-22 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601930054/Rugby%20Dashboard/rugby-dashboard-featured.png
color: "#e56816"
featured: true
keywords:
  - Dashboard
  - Information Architecture"
  - Interaction Design
---

## About the project

How to view a large amount of data related to a specific game while making it readable, easy to understand and relevant every step of the game. I was responsible for the whole UX and UI of this project. It was a quick challenge project done after hours in 2 weeks.

### So here’s the idea

Thinking of the game as a recording, link the data to it so the user can rewind, play and fast forward the data for unprecedented control. The statistics would then change in time with the video as it plays.

### Putting ideas to paper

Firstly I set to work conceptualising. With the help of good old pen and paper, I played with several concepts of how to display all the information on the screen

Eventually, I decided for a tiled interface that can be built by the users to analyse the data most relevant to them.

From the initial concept, I then moved on to low fidelity wireframing digitally. This was the time to refine ideas I had sketched out on paper and see which could work better.

From the more accurate layout, I realised I needed more flexibility, so each card can have a wide or narrow width, among other adjustments and affordances.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930054/Rugby%20Dashboard/rugby-dashboard-1.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930054/Rugby%20Dashboard/rugby-dashboard-2.jpg)

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930054/Rugby%20Dashboard/rugby-dashboard-3.jpg)

### High fidelity

Once the concepts were matured, it came the time to worry about the details and visual design.

![Final dashboard design](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930054/Rugby%20Dashboard/rugby-dashboard-4.png)

![Clicking any title would enable editing](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930054/Rugby%20Dashboard/rugby-dashboard-5.jpg)
![Tiles were draggable to customise the layout](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930054/Rugby%20Dashboard/rugby-dashboard-6.jpg)
