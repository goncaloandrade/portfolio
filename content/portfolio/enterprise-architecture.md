---
layout: portfolio
title: Enterprise Architecture 2015
date: 2016-03-17 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929759/Enterprise%20Architecture/enterprise-architecture-featured.png
color: "#1c8b9c"
keywords:
  - Information Architecture
  - Desktop
  - Enterprise Architecture
---
## Enterprise Architecture

The desktop app allowed enterprise architects to put some intelligence behind the process diagrams they draw in Visio. Using the data repository set up in iServer their work becomes richer and more organised.

### What did I do?

I was responsible for the whole User Experience and Visual Design of this project, as well as implementing the front-end together with the developers.

## Putting ideas to paper

Early on the decision was made to stay close to Office 2013 in terms of look and feel. This is because iServer works very closely with Visio and other Office apps, so on the one hand users already know the interaction mode, on the other, it allows for a more seamless experience, without having to go back and forwards between 2 different interaction models.

So I got to work figuring out how to use and adopt Microsoft’s rules and components to the complex tasks that had to be achieved with iServer. I also created completely new components and made sure they integrated into the experience.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929759/Enterprise%20Architecture/enterprise-architecture-1.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929759/Enterprise%20Architecture/enterprise-architecture-2.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929759/Enterprise%20Architecture/enterprise-architecture-3.png)

## Prototype

The users can navigate large sets of data quickly and easily.

![A contextual panel can be opened on the right to see more information about a specific object selected without having to navigate away.](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929759/Enterprise%20Architecture/enterprise-architecture-4.png)
![A query builder was added to facilitate constructing complex filters.](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929759/Enterprise%20Architecture/enterprise-architecture-5.png)

## Final product

The redesign was completed and shipped to clients.

The feedback was very positive and the transition was smooth.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929759/Enterprise%20Architecture/enterprise-architecture-6.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929759/Enterprise%20Architecture/enterprise-architecture-7.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929759/Enterprise%20Architecture/enterprise-architecture-9.jpg)