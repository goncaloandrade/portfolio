---
layout: portfolio
title: Enterprise Architecture Web app
date: 2016-03-17 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929812/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-featured.png
color: grey
seoDescription: Web app version of iServer 2015, which allowed enterprise
  architects to put intelligence in the diagrams they draw in Visio. They could
  create people, places or IT which they could then use to create their
  diagrams, complete with all the extra information and even validations and
  some auto diagramming.
keywords:
  - Information Architecture
  - Web
  - Enterprise Architecture
---
This is the browser-based web app version of [Enterprise Architecture 2015](/portfolio/enterprise-architecture-2015/), which allowed enterprise architects to put some intelligence behind the process diagrams they draw in Visio.

They could create people (managers, employees, supervisors), places (stores, offices, warehouses) or even IT (software, licenses, machines) in the app which they could then use to create their diagrams, complete with all the extra information and even validations and some auto diagramming.

## What I did

I was responsible for the whole User Experience and Visual Design of this project, as well as implementing the front-end together with the developers.

## Discovery

Since this product was meant as the next version of [Enterprise Architecture 2015](/portfolio/enterprise-architecture-2015/), there was a drive to stay close to Office and Office 365 in terms of look and feel. That was because iServer works very closely with Visio and other Office apps, so on the one hand users already know the interaction mode. However, the web has a very different interaction model, so the UX had to be crafted to stand in that middle ground and provide a seamless experience across these media. So I got to work figuring out how to use and adopt Microsoft’s rules and components to the complex tasks that had to be achieved. The design that was in place when I took over the project was considered limiting and didn’t meet user needs.

Preexisting design

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929811/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-1.png)

## Putting ideas to paper

I suggested a system that allowed the users to filter down the content in a more organic way. The issue with the inflexibility of the earlier design came from clinging to how [Enterprise Architecture 2015](https://goncaloandrade.com/portfolio/enterprise-architecture-2015/) structured its data in folders, as it was a native desktop app.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929811/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-13.svg)

Main user flows on the app

So I began designing a more flexible system that would always have the filter present on the left-hand side menu, allowing the users to alter the context of the page they are viewing at any point.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929811/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-2.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929811/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-3.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929811/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-4.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929811/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-5.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929811/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-6.jpg)

## High fidelity

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929811/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-7.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929811/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-8.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929811/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-9.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929811/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-10.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929811/Enterprise%20Architecture%20Web%20App/enterprise-architecture-web-app-11.png)

## Final product

The result was an app that presented the user with access to the wealth of information they needed in the simplest and most immediate way possible.