---
layout: portfolio
title: Checkout Form
date: 2016-08-10 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929626/Checkout%20Form/checkout-form-featured.png
color: "#33CC99"
keywords:
  - Information Architecture
  - App
---
## Meet Samantha

She’s in an airport waiting to board a flight to Colorado for vacation. She has some time to kill before her flight, so she does some impulse shopping online. This is her first time purchasing anything on this site. Since she’s going on vacation for a week, she wants it to be delivered after she arrives back home.

Samantha has initiated the checkout process after viewing the dress in her cart. She needs to fill out the checkout form, which contains billing/shipping and payment info. Once she provides that information, the site will enable her to review the information before submitting the transaction.

### What did I do?

I was responsible for the whole UX and UI of this project. It was a quick challenge project done after hours in a few days.

The process needs to be quick and simple, so as to not get in her way. Mistakes and cognitive strain need to be minimised so the process can be smooth and flawless, and the client doesn’t drop out of the purchase journey.

The checkout form provided is lengthy and the lack of visual hierarchy makes it difficult to read. The 1st task then is to deconstruct it and arrange the information in a more human-friendly manner.

## Information Architecture

Firstly, the information was broken down into logical segments, away from any interface or possible implementation. This allowed me to consider it on it’s on, and organise it on a conceptual level.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929624/Checkout%20Form/checkout-form-1.svg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929624/Checkout%20Form/checkout-form-2.svg)

Once the schematic was done, I then rearranged the blocks of information to make it more intuitive.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929624/Checkout%20Form/checkout-form-3.svg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929624/Checkout%20Form/checkout-form-4.svg)

### User journey

Now I could design an interaction that would guide the user through the information grouped into these sections.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929624/Checkout%20Form/checkout-form-5.svg)

## Putting ideas to paper

Pen and paper helped me refine how the interaction should work and how the different form elements should behave.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929624/Checkout%20Form/checkout-form-6.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929624/Checkout%20Form/checkout-form-7.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929624/Checkout%20Form/checkout-form-8.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929624/Checkout%20Form/checkout-form-9.png)

## Final product

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929624/Checkout%20Form/checkout-form-10.png)