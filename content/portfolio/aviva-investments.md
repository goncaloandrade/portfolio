---
layout: portfolio
title: Aviva.co.uk Investments redesign
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929147/Aviva%20Investments/aviva-investments-feature_l0l068.png
color: "#f9bc06"
highlight: true
seoDescription: I had the chance to redesign the investments section of the UK
  website for Aviva. I streamlined the journey and simplified the pages.
keywords:
  - Financial
  - Web
  - Commercial
  - Workshop
  - Empathy Mapping
  - User Testing
date: 2018-04-05 10:45:06 +0000
---
## Redesign the public website

I had the chance to redesign the investments section of the UK website for Aviva. The previous one was confusing to the users and not structured consistently, with plenty of duplicated content and jargon.

The products themselves are quite complicated, so they needed to be explained in as simple terms as possible to people who are very distrusting of financial institutions.

### What I did

As the sole UX designer assigned to the project, I was responsible for the whole user experience. With the help of my wonderful colleagues, we were able to craft a simpler and friendlier experience.

## Discovery

This phase took a long time as we mapped the user journeys and mental models. We conducted a few workshops with stakeholders and SMEs to understand both the users and the products themselves.

![Photo by Jo Szczepanska, Unsplash](https://images.unsplash.com/photo-1542626991-cbc4e32524cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80)

After some long talks and an impressive amount of post-its, we managed to understand the products and agree on the journeys the users were meant to go on to apply for them. We managed to reduce the complexity we needed to deal with by a lot.

Funnily enough, it took us a long while before we realised we were assuming people knew all about that ISAs are and what the tax allowance means. I talk about this in more detail in an [article I wrote.](https://goncaloandrade.com/4-ux-lessons-can-learn-dungeons-dragons/)

![Aviva investments maps](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929147/Aviva%20Investments/aviva-investments-maps_xa4ogj.svg)

## Putting ideas to paper

With all this data I then started drafting the wireframes. I needed to also make sure it would look and feel consistent with the other pages that were being migrated onto the same platform. These other pages, however, were for insurance products, so they naturally had different needs to serve, making them different, but still within the same branded Aviva experience.

![Gateway page wireframe](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929147/Aviva%20Investments/aviva-investments-1_oodpfo.png)
![GIA product page with calculator wireframe](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929147/Aviva%20Investments/aviva-investments-2_s6ykg1.png)
![Transfer Wireframe](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929147/Aviva%20Investments/aviva-investments-3_rltkfk.png)
![S&S product page with calculator wireframe](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929147/Aviva%20Investments/aviva-investments-4_wdgepj.png)

We included some extra features to help the users with the parts they had the most difficulty with, such as a video explaining the product, a calculator to illustrate the fees which were hard to understand and a structured educational content section where the users could go and learn about investments.

## Final delivery

In the end, we had to make some concessions and deliver an MVP to go live, but thanks to all the work put in by everyone it still ended up much better than the previous one. Once the Visual Designer tidied up and put the finishing touches in, we were ready to go live.

![Aviva Investments Category page](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929147/Aviva%20Investments/aviva-investments-5_fdsu85.png)
![Aviva ISA Pgea](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929147/Aviva%20Investments/aviva-investments-6_rnlor0.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929147/Aviva%20Investments/aviva-investments-7_r9kokr.png)
![Aviva Investment account page](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929147/Aviva%20Investments/aviva-investments-8_ln6wbw.png)