---
layout: portfolio
title: Drawing and Sketching
date: 2009-05-08 10:45:06 +0000
categories: null
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929689/Drawing/drawing-featured.png
color: grey
keywords:
  - School
  - Art
---
## Mastering “pen & paper” sketching

While in university I took art classes for 3 years. What I learned here was not only professional sketching, but also techniques to do with composition, perception psychology and observation.

Here is a selection of the best work I produced during that time. You can see a mix of techniques and types of illustrations and drawings.

![Legs anatomy study](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929688/https://res.cloudinary.com/drzkcguqd/image/upload/v1601929688/Drawing/drawing-1.jpg)
![Cloth and face shading](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929688/Drawing/drawing-2.jpg)
![nude male shading practice](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929688/Drawing/drawing-3.jpg)
![charcoal shading practice](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929688/Drawing/drawing-4.jpg)
![facial modelling practice](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929688/Drawing/drawing-5.jpg)
![Ceramics still life](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929688/Drawing/drawing-6.jpg)
![Anatomic feet highlighting](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929688/Drawing/drawing-7.jpg)
![Anatomy & clothing shading](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929688/Drawing/drawing-8.jpg)
![Muscle anatomy practice](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929688/Drawing/drawing-9.jpg)