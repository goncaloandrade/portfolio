---
layout: portfolio
title: Retirement Aviva.co.uk redesign
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929500/Aviva%20Retirement/aviva-retirement-featured.png
color: "#009fb8"
highlight: true
keywords:
  - Financial
  - Web
  - Commercial
  - Workshop
  - Empathy Mapping
  - User Testing
featured: true
date: 2018-07-13 10:45:06 +0000
seoDescription: Redesign of the Retirement section on Aviva's public UK website.
  This work made hundreds of people's lives better and increased conversion and
  sales
---
## Redesign the retirement public website

I had the chance to redesign the pensions and retirement section of the UK website for Aviva. The previous one was confusing to the users and not structured consistently, with plenty of duplicated content and jargon. This old site was structured from the business point of view, listing out the products like a menu. Problem is, most people have no idea what pension they have or how to use it.

The products themselves are quite complicated, so they needed to be explained in as simple terms as possible to people who are very distrusting of financial institutions.

### What I did

As the lead UX designer assigned to the project, I was responsible for the user experience. I worked together with [Jahnavee](https://www.linkedin.com/in/jahnavee-chitte-5821b3b/) who had extensive knowledge on how to get money out of a pension. With the help of our wonderful colleagues, we were able to craft a simpler and friendlier experience.

## Discovery

This phase took a long time as we mapped the user journeys and mental models. We also found the sheer number of products and how they related to one another to be quite confusing. In fact, we seemed to get some conflicting information from the business experts themselves. So I Drew up a product map so we could all agree on the same thing using it as a discussion piece.

Aviva Retirement products map

![Product map](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929499/Aviva%20Retirement/aviva-retirement-1.png)

Workshops with stakeholders and SMEs were conducted to understand both the users and the products themselves.

![Tom Persona](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929500/Aviva%20Retirement/aviva-retirement-2.jpg)
![Georgina persona](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929499/Aviva%20Retirement/aviva-retirement-3.jpg)

We needed to run several workshops on this project, simply due to the massive scope. What became most apparent from those sessions was that we needed to shift the focus to how the users approached retirement.

Then we continued the discovery phase with several techniques (the images below are blurred for NDA purposes)

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929499/Aviva%20Retirement/aviva-retirement-4.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929499/Aviva%20Retirement/aviva-retirement-5.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929499/Aviva%20Retirement/aviva-retirement-6.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929499/Aviva%20Retirement/aviva-retirement-7.png)

## Finding solutions

Then we started exploring solutions with drafts and wireframes

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929499/Aviva%20Retirement/aviva-retirement-8.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929499/Aviva%20Retirement/aviva-retirement-9.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929499/Aviva%20Retirement/aviva-retirement-10.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929499/Aviva%20Retirement/aviva-retirement-11.png)

Eventually, we settled on considering how our clients deal with their pensions throughout their lives. After all, it’s something that’s a part of your life from when you start working (ideally) to after you leave it for your spouse or family.

Retirement timeline, from the user’s perspective

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929499/Aviva%20Retirement/aviva-retirement-12.png)

## Testing Assumptions

Our confidence in this assumption got verified when we went into the user testing labs. Participants understood the above as a timeline and found it easier to navigate.