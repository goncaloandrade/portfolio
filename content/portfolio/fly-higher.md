---
layout: portfolio
title: Fly Higher
date: 2012-10-12 10:45:06 +0000
categories: null
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929846/Fly%20Higher/fly-higher-featured.png
color: "#4d5ab3"
keywords:
  - Graphic Design
  - Logo
---
Inova+, A consultancy that helped other companies do projects for the European Union. This involved, amongst many other things, graphic design for all their communication materials. I helped Inova+ design logos and identities to use with these on several projects.

## The Challenge

The project branding was targeted at children, to encourage them to go into aviation. A program was presented in schools around the European Union to teach children.

### What the children want

A fun friendly way to learn about aviation and careers in the field.

Rather than boring the kids with exposition, the project aimed to present itself in a positive way so as to get the children to associate it and aviation, hence encouraging more of them to follow into the field.

### and the educators

To convey their ideas about the field clearly and accurately, while at the same time maintaining the children’s interest and positive attitude towards a future career in the air.

The brand needed to be professional enough to be taken seriously be adults, even so they could be proud of it.

The materials also need to be fun and stimulating for the children so they can get their attention and be memorable in an emotionally positive way.

## The concept

A simple and fun design was created to make it easier for children to empathize with. However, it still had to be serious enough to be used in official documentation and other materials.

After exploring several ideas, the final design ended up being this paper aeroplane, which is reminiscent of playtime as well as representing perfectly the reach from professional pilots to their future colleagues.

The brand proved to be successful with children and memorable with everyone.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929849/Fly%20Higher/fly-higher-1.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929849/Fly%20Higher/fly-higher-3.jpg)

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929849/Fly%20Higher/fly-higher-2.jpg)