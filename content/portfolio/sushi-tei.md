---
layout: portfolio
title: Sushi Tei
date: 2011-04-14 10:45:06 +0000
categories: null
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601930362/Sushi%20Tei/sushi-tei-featured.png
color: "#9e9e9e"
keywords:
  - Graphic Design
  - Logo
---
# Sushi Restaurant branding

Quick branding and menu design for a new Japanese restaurant

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930362/Sushi%20Tei/sushi-tei-1.jpg)

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930362/Sushi%20Tei/sushi-tei-2.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930362/Sushi%20Tei/sushi-tei-3.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930362/Sushi%20Tei/sushi-tei-4.png)