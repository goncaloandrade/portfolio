---
layout: portfolio
title: Mind Kiwi
date: 2013-12-18 10:45:06 +0000
categories: null
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929938/Mind%20Kiwi/mind-kiwi-featured.png
color: "#17825e"
keywords:
  - Graphic Design
  - Logo
---
## Mental health for everyone

This startup web platform aimed to provide mental health care online, allowing patients and therapists easier ways to meet and conduct their sessions through video conferencing, messaging and other means.

### What the patients want

A friendly welcoming service to help them, free from any preconceptions that might be attached to mental health. It needs to be a safe place.

### For the therapists

A reliable and trustworthy service they can use to run and manage their business, as well as engage with their patients and conduct their sessions easily and smoothly.

## The concept

The brand had to be simple, easy to understand and shed any preconceptions that might be attached with mental health.

I explored a variety of ideas and directions, with visual examples such as the ones you can see here. Once decided on a style, I worked on a set of concepts, refining based on feedback.

![Discarded organic logo](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929937/Mind%20Kiwi/mind-kiwi-1.jpg)

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929937/Mind%20Kiwi/mind-kiwi-2.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929937/Mind%20Kiwi/mind-kiwi-3.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929937/Mind%20Kiwi/mind-kiwi-4.png)

![Discarded geometric logo](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929937/Mind%20Kiwi/mind-kiwi-6.jpg)

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929937/Mind%20Kiwi/mind-kiwi-7.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929937/Mind%20Kiwi/mind-kiwi-8.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929937/Mind%20Kiwi/mind-kiwi-9.png)

## Final product

In the end the logo ended up with a geometric illustration of a kiwi, which is abstract yet still illustrative of the mascot. The lines complicating the geometry hint at the complexity of mental health issues without bringing them to the forefront, making the brand sympathetic and understanding of these issues.

A soft green was chosen as the brand colour as a soothing and therapeutical colour, much like the tones used in many hospitals and health care facilities. However, the colour is more saturated than in those venues to avoid the sterile and cold clinical feelings they might convey.

![Final Logo](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929937/Mind%20Kiwi/mind-kiwi-12.png)

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929937/Mind%20Kiwi/mind-kiwi-10.png)
![Business cards](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929937/Mind%20Kiwi/mind-kiwi-14.jpg)

![Site mockup](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929937/Mind%20Kiwi/mind-kiwi-13.jpg)