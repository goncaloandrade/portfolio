import Link from 'next/link'
import matter from 'gray-matter'
import ReactMarkdown from 'react-markdown'

export default function BlogPost({ siteTitle, frontmatter, markdownBody }) {
  if (!frontmatter) return <></>

  return (
    <>
        <Link href="/">
          <a>Back to post list</a>
        </Link>
        <article className="container">
          <h1>{frontmatter.title}</h1>
          <p>By {frontmatter.author}</p>
          <div>
            <ReactMarkdown source={markdownBody} />
          </div>
        </article>
      </>
  )
}

export async function getStaticProps({ ...ctx }) {
  const { postname } = ctx.params

  const content = await import(`../../content/portfolio/${postname}.md`)
  // const config = await import(`../../siteconfig.json`)
  const data = matter(content.default)

  return {
    props: {
      siteTitle: "Goncalo Andrade",
      frontmatter: data.data,
      markdownBody: data.content,
    },
  }
}

export async function getStaticPaths() {
  const blogSlugs = ((context) => {
    const keys = context.keys()
    const data = keys.map((key, index) => {
      let slug = key.replace(/^.*[\\\/]/, '').slice(0, -3)

      return slug
    })
    return data
  })(require.context('../../content/portfolio', true, /\.md$/))

  const paths = blogSlugs.map((slug) => `/portfolio/${slug}`)

  return {
    paths,
    fallback: false,
  }
}