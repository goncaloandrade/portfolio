const Portfolio = () => {

}

export async function getStaticProps(x) {
  console.log("🚀 ~ getStaticProps ~ x", x);
  return {}
}

export async function getStaticPaths() {
  const fs = require('fs'),
    paths = [];

  fs.readdirSync("./content/portfolio").forEach(f => {
    const fName = f.replace(/.md$/g)
    paths.push({params:{id: f}});
  });
  
  return {
    paths,
    fallback: false
  }
}

export default Portfolio;