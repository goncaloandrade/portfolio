import matter from 'gray-matter';
import Head from 'next/head'
import Tile from '../components/Tile/Tile';

export default function Home({files}) {
  return (
    <div className="container">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      {files.map((x, k) => <Tile key={k} data={x} />)}
    </div>
  )
}

export async function getStaticProps(x) {
  const fs = require('fs'),
    path = "./content/portfolio",
    docs = await fs.readdirSync(path),
    files = docs.map(x => {
      const doc = matter.read(`${path}/${x}`);

      return {
        ...doc.data,
        slug: x.replace(/.md$/g, ""),
        content: doc.content
      }
    }),
    featuredFiles = files.filter(x => x.featured);

  return {
    props: {
      files: featuredFiles
    }
  };
}
