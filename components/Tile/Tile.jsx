import Link from "next/link";
import style from "./style.module.scss";

const Tile = ({data}) => {
	return <Link href={`/portfolio/${data.slug}`}>
		<a className={style.tileContainer} style={{
				backgroundImage: `url(${data.image})`,
				backgroundColor: data.color
			}}>
			<h1>{data.title}</h1>
		</a>
	</Link>
}

export default Tile;